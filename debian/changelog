libtie-ical-perl (0.15-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtie-ical-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 11:26:55 +0100

libtie-ical-perl (0.15-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on libmodule-build-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 20:48:31 +0100

libtie-ical-perl (0.15-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 15:00:24 +0100

libtie-ical-perl (0.15-2) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Fri, 29 May 2015 19:36:23 +0200

libtie-ical-perl (0.15-1) unstable; urgency=low

  * Imported Upstream version 0.15
  * Update d/copyright according to latest DEP5 revision
  * Refreshed 01-fixspelling.diff patch
  * Removed test.ics file using debian/clean
  * Added libmodule-build-perl (>= 0.380000) | perl (>= 5.13.11) to B-D

 -- Fabrizio Regalli <fabreg@fabreg.it>  Thu, 10 Nov 2011 11:48:40 +0100

libtie-ical-perl (0.14-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed:
    Homepage pseudo-field (Description); XS-Vcs-Svn fields.
    (Closes: #615329)
  * Split out patches in samples.
  * Refresh debian/rules, no functional changes; except: don't install minimal
    README any more.
  * debian/watch: use dist-based URL.
  * debian/copyright: wrap a long line; point to generic upstream source
    location.
  * Email change: gregor herrmann -> gregoa@debian.org

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Fabrizio Regalli ]
  * New upstream release
  * Upgrade d/compat to 8
  * Upgrade debhelper to (>= 8)
  * Bump Standards-Version to 3.9.2
  * Switch to DEP5 copyright format
  * Added myself to Uploaders
  * Added libtie-ical-perl.docs file for TODO
  * Added libtie-ical-perl.examples file for examples
  * Switch to 3.0 quilt format
  * Update copyright year
  * Removed hashbang.patch patch in favour of override_dh*
    commands
  * Added 01-fixspelling.diff patch

  [ gregor herrmann ]
  * Make short description a noun phrase.
  * Remove libmodule-build-perl from Build-Depends (in core since 5.9.4).

 -- Fabrizio Regalli <fabreg@fabreg.it>  Thu, 11 Aug 2011 15:00:04 +0200

libtie-ical-perl (0.13-1) unstable; urgency=low

  * Initial Release (closes: #313389).

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Sun,  9 Apr 2006 15:18:37 +0200
